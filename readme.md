<!--
SPDX-FileCopyrightText: Martin Jäger <martin@libre.solar>
SPDX-FileCopyrightText: Michel Langhammer <michel@libre.solar>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

## Pen Plotter - Hardware Description 


 Pen plotter is Arduino based machine for G-code drawings. 
 Simple kit and open source code for building and replication at schools ,universities, fablabs for implementation robotics courses.
 
![picture of assembled machien](<pen_plotter.png>)
**Technology:**

- 12V, 5A, 60w power supply
- NEMA17 stepper motors 
- Arduino Uno + CNC Shield v.3
- Plywood table 
- PLA/PETG 3-d printed parts  
- Dimensions approx. 40x40x20
(Dimensions when moving: 70x40x20)
- Weight approx. 3 kg

## Key Ressources
- [BOM file](bom.csv) 
- Design Files 
    - [Mechanical](/design files)
    - [Electronic](/design files)
- [Firmware]()
- [Manufacturing Documentation]()
- Build Workshop Documentation
    - [Step-by-Step-Guide](/manuals/Pen-Plotter-build-manual.pdf)
## License

Hardware design, CAD and PCB files, BOM, settings and other technical or design files are released under the following license:
- CERN Open Hardware Licence Version 2 Weakly Reciprocal - **[CERN-OHL-W](/LICENSES/CERN-OHL-W-2.0.txt)**

Assembly manual, pictures, videos, presentations, description text and other type of media are released under the following license:
- Creative-Commons-Attribution-ShareAlike 4.0 International - **[CC BY-SA 4.0](/LICENSES/CC-BY-SA-4.0.txt)**
